package com.belarizki.firebasecloudmesseging.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class TokenResponse(

    @SerializedName("error")
    @Expose
    var error: Boolean? = null,

    @SerializedName("messege")
    @Expose
    var messege: String? = null,

    @SerializedName("token")
    @Expose
    var token: String? = null

)