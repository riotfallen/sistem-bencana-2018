package com.belarizki.firebasecloudmesseging.api

import com.belarizki.firebasecloudmesseging.model.TokenResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface APIRepository {

    @GET("register-token.php")
    fun registerToken(@Query("token") token: String) : Call<TokenResponse>
}