package com.belarizki.firebasecloudmesseging.presenter

import android.util.Log
import com.belarizki.firebasecloudmesseging.api.APIRepository
import com.belarizki.firebasecloudmesseging.api.Client
import com.belarizki.firebasecloudmesseging.model.TokenResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TokenPresenter {

    fun registerToken(token: String){
        val apiRepository = Client.getClient().create(APIRepository::class.java)
        val call: Call<TokenResponse> = apiRepository.registerToken(token)
        call.enqueue(object : Callback<TokenResponse>{
            override fun onFailure(call: Call<TokenResponse>, t: Throwable) {
                Log.e("TOKEN", "ERROR TO REGIST TOKEN : " + t.message)
            }

            override fun onResponse(call: Call<TokenResponse>, response: Response<TokenResponse>) {
                Log.d("TOKEN", "TOKEN REGISTERED TO SERVER")
            }

        })
    }
}