package com.belarizki.firebasecloudmesseging.utils

import android.content.Context
import android.content.SharedPreferences
import com.belarizki.firebasecloudmesseging.R

class PrefConfig(private val context: Context,
                 private val sharedPreferences: SharedPreferences =
                     context.getSharedPreferences(context.getString(R.string.pref_fcm), Context.MODE_PRIVATE)) {

    fun writeToken(token: String?){
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(context.getString(R.string.pref_token_fcm), token)
        editor.apply()
    }

    fun readToken() : String? = sharedPreferences.getString(context.getString(R.string.pref_token_fcm), "")
}