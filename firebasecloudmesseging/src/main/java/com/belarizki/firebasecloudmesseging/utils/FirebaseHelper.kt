package com.belarizki.firebasecloudmesseging.utils

import android.content.Context

class FirebaseHelper(private val context: Context) {

    fun getToken(): String? =  PrefConfig(context).readToken()

}