package com.belarizki.firebasecloudmesseging.services

import android.util.Log
import com.belarizki.firebasecloudmesseging.presenter.TokenPresenter
import com.belarizki.firebasecloudmesseging.utils.NotificationHelper
import com.belarizki.firebasecloudmesseging.utils.PrefConfig
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingServices : FirebaseMessagingService(){

    private lateinit var tokenPresenter: TokenPresenter

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        PrefConfig(applicationContext).writeToken(token)
        Log.d("NEW_TOKEN", token)
        tokenPresenter = TokenPresenter()

        if (token != null) {
            tokenPresenter.registerToken(token)
        }
    }

    override fun onMessageReceived(p0: RemoteMessage?) {
        showNotification(p0?.notification)
    }

    private fun showNotification(notification: RemoteMessage.Notification?) {
        NotificationHelper(applicationContext)
            .createNotification(notification?.title, notification?.body)
    }
}