package com.belarizki.firebasecloudmesseging.view

import com.belarizki.firebasecloudmesseging.model.TokenResponse

interface TokenView {

    fun showTokenResponse(response: TokenResponse)
}