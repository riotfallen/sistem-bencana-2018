package com.belarizki.authentication.utils

import android.content.Context
import android.content.SharedPreferences
import com.belarizki.authentication.R

class PrefConfig(
    private val context: Context,
    private val sharedPreferences: SharedPreferences = context.getSharedPreferences(
        context.getString(R.string.pref_auth),
        Context.MODE_PRIVATE
    )
) {
    fun writeTokenLogin(token: String){
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(context.getString(R.string.pref_token_login), token)
        editor.apply()
    }

    fun readTokenLogin() : String? = sharedPreferences.getString(context.getString(R.string.pref_token_login), "")
}