package com.belarizki.authentication.api

import com.belarizki.authentication.model.AuthResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface APIRepository {

    @FormUrlEncoded
    @POST("register.php")
    fun doRegister(
        @Field("firstName") firstName: String,
        @Field("lastName") lastName: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("alamat") alamat: String,
        @Field("kelurahan") kelurahan: String,
        @Field("url") url: String
    ): Call<AuthResponse>

    @FormUrlEncoded
    @POST("login.php")
    fun doLogin(
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<AuthResponse>
}