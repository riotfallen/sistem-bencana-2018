package com.belarizki.authentication.view

import com.belarizki.authentication.model.AuthResponse

interface AuthView {
    fun showAuthLoading()
    fun hideAuthLoading()
    fun showFailureMessege(messege: String, errorCode: Int = 500)
    fun showAuthData(data: AuthResponse)
}