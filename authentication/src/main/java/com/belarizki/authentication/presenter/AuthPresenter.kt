package com.belarizki.authentication.presenter

import com.belarizki.authentication.api.APIRepository
import com.belarizki.authentication.api.Client
import com.belarizki.authentication.model.AuthResponse
import com.belarizki.authentication.view.AuthView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthPresenter(private val view: AuthView) {

    fun getAuth(
        firstName: String,
        lastName: String,
        email: String,
        password: String,
        alamat: String,
        kelurahan: String,
        url: String
    ) {
        view.showAuthLoading()
        val apiRepository = Client.getClient().create(APIRepository::class.java)
        val call: Call<AuthResponse> =
            apiRepository.doRegister(firstName, lastName, email, password, alamat, kelurahan, url)
        call.enqueue(object : Callback<AuthResponse> {
            override fun onFailure(call: Call<AuthResponse>, t: Throwable) {
                t.message?.let { view.showFailureMessege(it) }
                view.hideAuthLoading()
            }

            override fun onResponse(call: Call<AuthResponse>, response: Response<AuthResponse>) {
                if (response.isSuccessful) {
                    val data = response.body()
                    if (!data?.error!!) {
                        view.showAuthData(data)
                        view.hideAuthLoading()
                    } else {
                        data.messege.let { view.showFailureMessege(it) }
                        view.hideAuthLoading()
                    }
                } else {
                    view.showFailureMessege(response.raw().message())
                    view.hideAuthLoading()
                }
            }

        })
    }

    fun getAuth(email: String, password: String) {
        view.showAuthLoading()
        val apiRepository = Client.getClient().create(APIRepository::class.java)
        val call: Call<AuthResponse> = apiRepository.doLogin(email, password)
        call.enqueue(object : Callback<AuthResponse> {
            override fun onFailure(call: Call<AuthResponse>, t: Throwable) {
                t.message?.let { view.showFailureMessege(it) }
                view.hideAuthLoading()
            }

            override fun onResponse(call: Call<AuthResponse>, response: Response<AuthResponse>) {
                if (response.isSuccessful) {
                    val data = response.body()
                    if (!data?.error!!) {
                        view.showAuthData(data)
                        view.hideAuthLoading()
                    } else {
                        data.messege.let { view.showFailureMessege(it) }
                        view.hideAuthLoading()
                    }
                } else {
                    view.showFailureMessege(response.raw().message())
                    view.hideAuthLoading()
                }
            }

        })
    }
}