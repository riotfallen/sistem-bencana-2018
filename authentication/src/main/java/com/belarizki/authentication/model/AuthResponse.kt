package com.belarizki.authentication.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class AuthResponse(

    @SerializedName("error")
    @Expose
    var error: Boolean,

    @SerializedName("messege")
    @Expose
    var messege: String,

    @SerializedName("auth")
    @Expose
    var auth: Auth? = null

)