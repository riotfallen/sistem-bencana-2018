package com.belarizki.authentication.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Auth(
    @SerializedName("userId")
    @Expose
    var userId: String? = null,

    @SerializedName("fistName")
    @Expose
    var fistName: String? = null,

    @SerializedName("lastName")
    @Expose
    var lastName: String? = null,

    @SerializedName("email")
    @Expose
    var email: String? = null,

    @SerializedName("avatar")
    @Expose
    var avatar: String? = null,

    @SerializedName("token")
    @Expose
    var token: String? = null
)