package com.belarizki.core.presenter.geo

import com.belarizki.core.api.APIRepository
import com.belarizki.core.api.Client
import com.belarizki.core.model.geo.kota.KotaResponse
import com.belarizki.core.view.geo.KotaView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class KotaPresenter(private val view: KotaView) {

    fun getKota(provinsiId: String){
        view.showKotaLoading()
        val apiRepository: APIRepository = Client.getClient().create(APIRepository::class.java)
        val call: Call<KotaResponse> = apiRepository.getKota(provinsiId, 1)
        call.enqueue(object : Callback<KotaResponse>{
            override fun onFailure(call: Call<KotaResponse>, t: Throwable) {
                view.hideKotaLoading()
                t.message?.let { view.showKotaError(it) }
            }

            override fun onResponse(call: Call<KotaResponse>, response: Response<KotaResponse>) {
                view.hideKotaLoading()
                val data = response.body()
                when(data?.error){
                    true -> view.showKotaError(data.messege)
                    else -> data?.kota?.let { view.showKotaData(it) }
                }
            }

        })
    }

}