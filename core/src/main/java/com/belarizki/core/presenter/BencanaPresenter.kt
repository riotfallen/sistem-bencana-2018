package com.belarizki.core.presenter

import com.belarizki.core.api.APIRepository
import com.belarizki.core.api.Client
import com.belarizki.core.model.bencana.BencanaResponse
import com.belarizki.core.view.bencana.BencanaView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BencanaPresenter(private val view: BencanaView){

    fun getBencana(){
        view.showBencanaLoading()
        val apiRepository: APIRepository = Client.getClient().create(APIRepository::class.java)
        val call: Call<BencanaResponse> = apiRepository.getBencana()
        call.enqueue(object : Callback<BencanaResponse>{
            override fun onFailure(call: Call<BencanaResponse>, t: Throwable) {
                view.hideBencanaLoading()
                t.message?.let { view.showBencanaError(it, 500) }
            }

            override fun onResponse(call: Call<BencanaResponse>, response: Response<BencanaResponse>) {
                val data = response.body()
                view.hideBencanaLoading()
                when(data?.error){
                    true -> data.message?.let { view.showBencanaError(it, 500) }
                    false -> data.bencana?.let { view.showBencanaData(it) }
                }
            }

        })
    }

}