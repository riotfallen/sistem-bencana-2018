package com.belarizki.core.presenter.geo

import com.belarizki.core.api.APIRepository
import com.belarizki.core.api.Client
import com.belarizki.core.model.geo.provinsi.ProvinsiResponse
import com.belarizki.core.utils.PROV_ID_JABAR
import com.belarizki.core.view.geo.ProvinsiView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProvinsiPresenter(private val view: ProvinsiView) {

    fun getProvinsi(){
        view.showProvinsiLoading()
        val apiRepository: APIRepository = Client.getClient().create(APIRepository::class.java)
        val call: Call<ProvinsiResponse> = apiRepository.getProvinsi(PROV_ID_JABAR)
        call.enqueue(object : Callback<ProvinsiResponse>{
            override fun onFailure(call: Call<ProvinsiResponse>, t: Throwable) {
                view.hideProvinsiLoading()
                t.message?.let { view.showProvinsiError(it) }
            }

            override fun onResponse(call: Call<ProvinsiResponse>, response: Response<ProvinsiResponse>) {
                view.hideProvinsiLoading()
                val data = response.body()
                when(data?.error){
                    true -> view.showProvinsiError(data.messege)
                    false -> data.provinsi?.let { view.showProvinsiData(it) }
                }
            }

        })
    }

}