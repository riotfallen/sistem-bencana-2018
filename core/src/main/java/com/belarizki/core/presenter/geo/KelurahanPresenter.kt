package com.belarizki.core.presenter.geo

import com.belarizki.core.api.APIRepository
import com.belarizki.core.api.Client
import com.belarizki.core.model.geo.kelurahan.KelurahanResponse
import com.belarizki.core.view.geo.KelurahanView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class KelurahanPresenter(private val view: KelurahanView) {

    fun getKelurahan(kecamatanId: String){
        view.showKelurahanLoading()
        val apiRepository: APIRepository = Client.getClient().create(APIRepository::class.java)
        val call: Call<KelurahanResponse> = apiRepository.getKelurahan(kecamatanId)
        call.enqueue(object : Callback<KelurahanResponse>{
            override fun onFailure(call: Call<KelurahanResponse>, t: Throwable) {
                view.hideKelurahanLoading()
                t.message?.let { view.showKelurahanError(it) }
            }

            override fun onResponse(call: Call<KelurahanResponse>, response: Response<KelurahanResponse>) {
                view.hideKelurahanLoading()
                val data = response.body()
                when(data?.error){
                    true -> view.showKelurahanError(data.messege)
                    false -> data.kelurahan?.let { view.showKelurahanData(it) }
                }
            }

        })
    }

}