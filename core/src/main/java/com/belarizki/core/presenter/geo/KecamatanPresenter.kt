package com.belarizki.core.presenter.geo

import com.belarizki.core.api.APIRepository
import com.belarizki.core.api.Client
import com.belarizki.core.model.geo.kecamatan.KecamatanResponse
import com.belarizki.core.view.geo.KecamatanView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class KecamatanPresenter(private val view: KecamatanView) {

    fun getKecamatan(kotaId: String){
        view.showKecamatanLoading()
        val apiRepository: APIRepository = Client.getClient().create(APIRepository::class.java)
        val call: Call<KecamatanResponse> = apiRepository.getKecamatan(kotaId)
        call.enqueue(object : Callback<KecamatanResponse>{
            override fun onFailure(call: Call<KecamatanResponse>, t: Throwable) {
                view.hideKecamatanLoading()
                t.message?.let { view.showKecamatanError(it) }
            }

            override fun onResponse(call: Call<KecamatanResponse>, response: Response<KecamatanResponse>) {
                view.hideKecamatanLoading()
                val data = response.body()
                when(data?.error){
                    true -> view.showKecamatanError(data.messege)
                    false -> data.kecamatan?.let { view.showKecamatanData(it) }
                }
            }

        })
    }

}