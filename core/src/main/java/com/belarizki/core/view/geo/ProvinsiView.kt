package com.belarizki.core.view.geo

import com.belarizki.core.model.geo.provinsi.Provinsi

interface ProvinsiView {
    fun showProvinsiLoading()
    fun hideProvinsiLoading()
    fun showProvinsiData(data: MutableList<Provinsi>)
    fun showProvinsiError(messege: String, errorCode: Int = 500)
}