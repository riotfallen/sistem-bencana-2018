package com.belarizki.core.view.geo

import com.belarizki.core.model.geo.kota.Kota

interface KotaView {
    fun showKotaLoading()
    fun hideKotaLoading()
    fun showKotaData(data: MutableList<Kota>)
    fun showKotaError(messege: String)
}