package com.belarizki.core.view.bencana

import com.belarizki.core.model.bencana.Bencana

interface BencanaView {
    fun showBencanaLoading()
    fun hideBencanaLoading()
    fun showBencanaError(message: String, errorCode: Int)
    fun showBencanaData(data: MutableList<Bencana>)
}