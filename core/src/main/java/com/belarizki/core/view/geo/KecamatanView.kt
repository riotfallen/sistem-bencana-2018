package com.belarizki.core.view.geo

import com.belarizki.core.model.geo.kecamatan.Kecamatan

interface KecamatanView {

    fun showKecamatanLoading()
    fun hideKecamatanLoading()
    fun showKecamatanData(data: MutableList<Kecamatan>)
    fun showKecamatanError(messege: String)

}