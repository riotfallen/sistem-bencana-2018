package com.belarizki.core.view.geo

import com.belarizki.core.model.geo.kelurahan.Kelurahan

interface KelurahanView {
    fun showKelurahanLoading()
    fun hideKelurahanLoading()
    fun showKelurahanData(data: MutableList<Kelurahan>)
    fun showKelurahanError(messege: String, errorCode: Int = 500)
}