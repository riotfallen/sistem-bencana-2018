package com.belarizki.core.api

import com.belarizki.core.model.bencana.BencanaResponse
import com.belarizki.core.model.geo.kecamatan.KecamatanResponse
import com.belarizki.core.model.geo.kelurahan.KelurahanResponse
import com.belarizki.core.model.geo.kota.KotaResponse
import com.belarizki.core.model.geo.provinsi.ProvinsiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface APIRepository {

    @GET("prov.php")
    fun getProvinsi(@Query("id") id: Int): Call<ProvinsiResponse>

    @GET("kota.php")
    fun getKota(@Query("prov") provinsi: String,
                @Query("limit") limit: Int) : Call<KotaResponse>

    @GET("kecamatan.php")
    fun getKecamatan(@Query("kota") kota: String) : Call<KecamatanResponse>

    @GET("kelurahan.php")
    fun getKelurahan(@Query("kec") kecamatan: String) : Call<KelurahanResponse>

    @GET("bencana-lookup.php")
    fun getBencana() : Call<BencanaResponse>
}

