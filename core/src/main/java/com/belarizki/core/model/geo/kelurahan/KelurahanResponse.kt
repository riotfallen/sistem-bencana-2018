package com.belarizki.core.model.geo.kelurahan

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class KelurahanResponse (

    @SerializedName("error")
    @Expose
    var error: Boolean,
    @SerializedName("messege")
    @Expose
    var messege: String,
    @SerializedName("kelurahan")
    @Expose
    var kelurahan: MutableList<Kelurahan>? = null

)