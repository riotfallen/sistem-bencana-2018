package com.belarizki.core.model.bencana

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class BencanaResponse (

    @SerializedName("error")
    @Expose
    var error: Boolean? = null,

    @SerializedName("message")
    @Expose
    var message: String? = null,

    @SerializedName("bencana")
    @Expose
    var bencana: MutableList<Bencana>? = null

)