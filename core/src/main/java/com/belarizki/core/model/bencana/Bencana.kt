package com.belarizki.core.model.bencana

import com.belarizki.core.model.general.Picture
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Bencana(

    @SerializedName("id")
    @Expose
    var id: String? = null,

    @SerializedName("tipe_bencana")
    @Expose
    var tipeBencana: String? = null,

    @SerializedName("judul")
    @Expose
    var judul: String? = null,

    @SerializedName("tanggal_post")
    @Expose
    var tanggalPost: String? = null,

    @SerializedName("deskripsi")
    @Expose
    var deskripsi: String? = null,

    @SerializedName("alamat_lengkap")
    @Expose
    var alamatLengkap: String? = null,

    @SerializedName("latitude")
    @Expose
    var latitude: String? = null,

    @SerializedName("longitude")
    @Expose
    var longitude: String? = null,

    @SerializedName("tanggal")
    @Expose
    var tanggal: String? = null,

    @SerializedName("bulan")
    @Expose
    var bulan: String? = null,

    @SerializedName("tahun")
    @Expose
    var tahun: String? = null,

    @SerializedName("userId")
    @Expose
    var userId: String? = null,

    @SerializedName("report")
    @Expose
    var report: String? = null,

    @SerializedName("isPosted")
    @Expose
    var isPosted: String? = null,

    @SerializedName("jumlah_korban")
    @Expose
    var jumlahKorban: String? = null,

    @SerializedName("jumlah_luka")
    @Expose
    var jumlahLuka: String? = null,

    @SerializedName("jumlah_hilang")
    @Expose
    var jumlahHilang: String? = null,

    @SerializedName("strength")
    @Expose
    var strength: String? = null,

    @SerializedName("pictures")
    @Expose
    var pictures: MutableList<Picture>? = null,

    @SerializedName("korban")
    @Expose
    var korban: MutableList<Korban>? = null
)
