package com.belarizki.core.model.bencana

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Korban(

    @SerializedName("id")
    @Expose
    val id: String? = null,

    @SerializedName("nama")
    @Expose
    val nama: String? = null,

    @SerializedName("kelamin")
    @Expose
    val kelamin: String? = null

)