package com.belarizki.core.model.geo.kota

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class KotaResponse (

    @SerializedName("error")
    @Expose
    var error: Boolean,

    @SerializedName("messege")
    @Expose
    var messege: String,

    @SerializedName("kota")
    @Expose
    var kota: MutableList<Kota>? = null

)