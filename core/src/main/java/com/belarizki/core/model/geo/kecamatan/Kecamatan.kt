package com.belarizki.core.model.geo.kecamatan

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Kecamatan (

    @SerializedName("id")
    @Expose
    var id: String? = null,

    @SerializedName("nama")
    @Expose
    var nama: String? = null

)