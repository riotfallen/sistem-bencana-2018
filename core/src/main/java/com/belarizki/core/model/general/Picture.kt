package com.belarizki.core.model.general

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Picture(
    @SerializedName("id")
    @Expose
    val id: String? = null,

    @SerializedName("deskripsi")
    @Expose
    val deskripsi: String? = null,

    @SerializedName("url")
    @Expose
    val url: String? = null
)