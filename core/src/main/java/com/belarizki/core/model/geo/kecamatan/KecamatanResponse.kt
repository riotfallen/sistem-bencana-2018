package com.belarizki.core.model.geo.kecamatan

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class KecamatanResponse (

    @SerializedName("error")
    @Expose
    var error: Boolean,

    @SerializedName("messege")
    @Expose
    var messege: String,

    @SerializedName("kecamatan")
    @Expose
    var kecamatan: MutableList<Kecamatan>? = null

)