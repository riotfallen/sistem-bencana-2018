package com.belarizki.core.model.geo.provinsi

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class ProvinsiResponse (

    @SerializedName("error")
    @Expose
    var error: Boolean,

    @SerializedName("messege")
    @Expose
    var messege: String,

    @SerializedName("provinsi")
    @Expose
    var provinsi: MutableList<Provinsi>? = null

)