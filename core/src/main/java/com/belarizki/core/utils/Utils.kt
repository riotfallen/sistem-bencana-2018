package com.belarizki.core.utils

import android.view.View

fun View.visible(){
    visibility = View.VISIBLE
}

fun View.invisible(){
    visibility = View.INVISIBLE
}

fun  View.gone(){
    visibility = View.GONE
}

const val PROV_ID_JABAR = 8