package com.belarizki.core.utils

import android.content.Context
import android.content.SharedPreferences
import com.belarizki.core.R

class PrefConfig(private val context: Context,
                 private val sharedPreference: SharedPreferences =
                     context.getSharedPreferences(context.getString(R.string.pref_core),
                         Context.MODE_PRIVATE)) {

    fun setFirstLaunch(isFistLaunch: Boolean){
        val editor: SharedPreferences.Editor = sharedPreference.edit()
        editor.putBoolean(context.getString(R.string.pref_first_launch), isFistLaunch)
        editor.apply()
    }

    fun isFirstLaunch() : Boolean = sharedPreference.getBoolean(context.getString(R.string.pref_first_launch), true)
}