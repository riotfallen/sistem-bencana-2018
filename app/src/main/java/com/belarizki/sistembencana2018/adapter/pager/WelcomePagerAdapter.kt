package com.belarizki.sistembencana2018.adapter.pager

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v4.view.ViewPager


class WelcomePagerAdapter(private val context: Context,
                          private val noOfTabs: IntArray) : PagerAdapter() {

    private lateinit var layoutInflater: LayoutInflater

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater.inflate(noOfTabs[position], container, false)
        container.addView(view)
        return view
    }

    override fun isViewFromObject(view: View, objects: Any): Boolean = view == objects

    override fun getCount(): Int = noOfTabs.size

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }

}