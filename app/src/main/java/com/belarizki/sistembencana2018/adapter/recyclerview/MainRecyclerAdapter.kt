package com.belarizki.sistembencana2018.adapter.recyclerview

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.belarizki.core.model.bencana.Bencana
import com.belarizki.sistembencana2018.BuildConfig
import com.belarizki.sistembencana2018.R
import com.squareup.picasso.Picasso
import org.jetbrains.anko.toast

class MainRecyclerAdapter(private val context: Context, private val data: MutableList<Bencana>) :
    RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.recycler_item_bencana, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(data[position])
        holder.itemView.setOnClickListener {
            context.toast("you clicking: ${data[position].judul}")
        }
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val imageViewBencana: ImageView = view.findViewById(R.id.ribImageViewBencana)
        private val textViewJudul: TextView = view.findViewById(R.id.ribTextViewJudulBencana)
        private val textViewDescription: TextView = view.findViewById(R.id.ribTextViewDetailBencana)
        private val textViewLokasi: TextView = view.findViewById(R.id.ribTextViewLokasiBencana)
        private val textViewJumlahKorban: TextView = view.findViewById(R.id.ribTextViewJumlahKorban)

        fun onBind(bencana: Bencana) {
            textViewJudul.text = bencana.judul
            textViewDescription.text = bencana.deskripsi
            textViewLokasi.text = bencana.alamatLengkap
            textViewJumlahKorban.text = bencana.jumlahKorban

            Picasso.get().load(BuildConfig.BASE_URL + bencana.pictures?.get(0)?.url).fit().centerCrop().into(imageViewBencana)

        }
    }

}