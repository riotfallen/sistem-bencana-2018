package com.belarizki.sistembencana2018.adapter.pager

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.belarizki.sistembencana2018.R
import com.belarizki.sistembencana2018.fragment.BencanaListFragment
import com.belarizki.sistembencana2018.fragment.MapBaseFragment

class MainPagerAdapter(private val context: Context, private val noOfTabs: Int, fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> BencanaListFragment()
            else -> MapBaseFragment()
        }
    }

    override fun getCount(): Int  = noOfTabs

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position) {
            0 -> context.getString(R.string.daftar_bencana)
            else -> context.getString(R.string.peta_bencana)
        }
    }
}