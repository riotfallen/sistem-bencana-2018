package com.belarizki.sistembencana2018.activity.authentication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.belarizki.authentication.model.AuthResponse
import com.belarizki.authentication.presenter.AuthPresenter
import com.belarizki.authentication.view.AuthView
import com.belarizki.core.model.geo.kecamatan.Kecamatan
import com.belarizki.core.model.geo.kelurahan.Kelurahan
import com.belarizki.core.model.geo.kota.Kota
import com.belarizki.core.presenter.geo.KecamatanPresenter
import com.belarizki.core.presenter.geo.KelurahanPresenter
import com.belarizki.core.presenter.geo.KotaPresenter
import com.belarizki.core.utils.invisible
import com.belarizki.core.utils.visible
import com.belarizki.core.view.geo.KecamatanView
import com.belarizki.core.view.geo.KelurahanView
import com.belarizki.core.view.geo.KotaView
import com.belarizki.sistembencana2018.R
import com.belarizki.sistembencana2018.utils.JABAR_ID
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast
import android.widget.AdapterView.OnItemSelectedListener



class RegisterActivity : AppCompatActivity(), AuthView, KotaView, KecamatanView, KelurahanView {


    private lateinit var authPresenter: AuthPresenter
    private lateinit var kotaPresenter: KotaPresenter
    private lateinit var kecamatanPresenter: KecamatanPresenter
    private lateinit var kelurahanPresenter: KelurahanPresenter

    private var listKelurahan: MutableList<Kelurahan> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        setSupportActionBar(registerActivityToolbar)
        supportActionBar?.title = "Registrasi"

        authPresenter = AuthPresenter(this)

        kotaPresenter = KotaPresenter(this)

        kotaPresenter.getKota(JABAR_ID)

        registerActivityButtonRegister.onClick {
            val firstName = registerActivityEditTextNamaDepan.text.toString()
            val lastName = registerActivityEditTextNamaBelakang.text.toString()
            val password = registerActivityEditTextPassword.text.toString()
            val email = registerActivityEditTextEmail.text.toString()
            val alamatLengkap = registerActivityEditTextAlamat.text.toString()
            val url = registerActivityURL.text.toString()
            val kelurahanId = listKelurahan[registerActivitySpinnerKelurahan.selectedItemPosition].id.toString()

            authPresenter.getAuth(firstName, lastName, email, password, alamatLengkap, kelurahanId, url)
        }

    }

    override fun showAuthLoading() {
        registerActivityProgressBar.visible()
        registerActivityButtonRegister.invisible()
    }

    override fun hideAuthLoading() {
        registerActivityProgressBar.invisible()
        registerActivityButtonRegister.visible()
    }

    override fun showFailureMessege(messege: String, errorCode: Int) {
        toast("$messege : $errorCode")
    }

    override fun showAuthData(data: AuthResponse) {
        toast("Success")
    }

    override fun showKotaData(data: MutableList<Kota>) {
        val spinnerArray: MutableList<String> = mutableListOf()
        for (kota in data) {
            kota.nama?.let { spinnerArray.add(it) }
        }

        spinnerFiller(spinnerArray, registerActivitySpinnerKota)
        registerActivitySpinnerKota.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                kecamatanPresenter = KecamatanPresenter(this@RegisterActivity)
                data[position].id?.let { kecamatanPresenter.getKecamatan(it) }
            }
        }
    }

    override fun showKotaError(messege: String) {
        toast(messege)
    }

    override fun showKecamatanData(data: MutableList<Kecamatan>) {
        val spinnerArray: MutableList<String> = mutableListOf()
        for(kecamatan in data){
            kecamatan.nama?.let { spinnerArray.add(it) }
        }

        spinnerFiller(spinnerArray, registerActivitySpinnerKecamatan)
        registerActivitySpinnerKecamatan.onItemSelectedListener = object: OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                kelurahanPresenter = KelurahanPresenter(this@RegisterActivity)
                data[position].id?.let { kelurahanPresenter.getKelurahan(it) }
            }

        }
    }

    override fun showKecamatanError(messege: String) {
        toast(messege)
    }


    override fun showKelurahanData(data: MutableList<Kelurahan>) {
        val spinnerArray: MutableList<String> = mutableListOf()
        for(kelurahan in data){
            kelurahan.nama?.let { spinnerArray.add(it) }
        }

        spinnerFiller(spinnerArray, registerActivitySpinnerKelurahan)

        listKelurahan = data
    }

    override fun showKelurahanError(messege: String, errorCode: Int) {
        toast(messege)
    }

    private fun spinnerFiller(spinnerArray: MutableList<String>, spinner: Spinner){
        val arrayAdapter = ArrayAdapter<String>(
            this, android.R.layout.simple_spinner_dropdown_item, spinnerArray
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = arrayAdapter
    }

    override fun showKecamatanLoading() {}
    override fun hideKecamatanLoading() {}
    override fun showKelurahanLoading() {}
    override fun hideKelurahanLoading() {}
    override fun showKotaLoading() {}
    override fun hideKotaLoading() {}
}
