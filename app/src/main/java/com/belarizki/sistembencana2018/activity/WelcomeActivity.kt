package com.belarizki.sistembencana2018.activity

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.text.Html
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import com.belarizki.core.utils.PrefConfig
import com.belarizki.core.utils.gone
import com.belarizki.core.utils.visible
import com.belarizki.sistembencana2018.R
import com.belarizki.sistembencana2018.adapter.pager.WelcomePagerAdapter
import kotlinx.android.synthetic.main.activity_welcome.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.textColor

class WelcomeActivity : AppCompatActivity() {

    private var layoutsWelcome = intArrayOf()
    private lateinit var dots: Array<TextView?>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_welcome)

        if(!PrefConfig(this).isFirstLaunch()){
            startActivity<MainActivity>()
            finish()
        }

        layoutsWelcome = intArrayOf(
            R.layout.welcome_screen_1,
            R.layout.welcome_screen_2,
            R.layout.welcome_screen_3
        )
        addButtonDots(0)

        welcomeActivityButtonNext.setOnClickListener {
            val current = getItem(1)
            if(current < layoutsWelcome.size)
                welcomeActivityViewPager.currentItem = current
        }

        welcomeActivityButtonSkip.setOnClickListener {
            PrefConfig(this).setFirstLaunch(false)
            startActivity<MainActivity>()
            finish()
        }

        val welcomePagerAdapter = WelcomePagerAdapter(this, layoutsWelcome)
        welcomeActivityViewPager.adapter = welcomePagerAdapter
        welcomeActivityViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(p0: Int) {}

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}

            override fun onPageSelected(position: Int) {
                addButtonDots(position)

                if (position == layoutsWelcome.size - 1){
                    welcomeActivityButtonNext.gone()
                    welcomeActivityButtonSkip.gone()
                } else {
                    welcomeActivityButtonNext.visible()
                    welcomeActivityButtonSkip.visible()
                }
            }
        })
    }

    private fun getItem(i: Int): Int = welcomeActivityViewPager.currentItem + i

    @Suppress("DEPRECATION")
    private fun addButtonDots(currentPage: Int){
        dots = arrayOfNulls(layoutsWelcome.size)

        welcomeActivityLayoutDots.removeAllViews()
        for(i in dots.indices){
            dots[i] = TextView(this)
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                dots[i]?.text = Html.fromHtml("&#8226", Html.FROM_HTML_MODE_LEGACY)
            } else {
                dots[i]?.text = Html.fromHtml("&#8226")
            }
            dots[i]?.textSize = 35f
            dots[i]?.textColor = resources.getColor(R.color.dot_dark_screen)
            welcomeActivityLayoutDots.addView(dots[i])

        }

        if(dots.isNotEmpty()){
           dots[currentPage]?.textColor = resources.getColor(R.color.dot_light_screen)
        }
    }
}
