package com.belarizki.sistembencana2018.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.belarizki.authentication.utils.PrefConfig
import com.belarizki.sistembencana2018.R
import com.belarizki.sistembencana2018.activity.authentication.LoginActivity
import com.belarizki.sistembencana2018.adapter.pager.MainPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val tokenLogin = PrefConfig(this).readTokenLogin()

        if (tokenLogin.isNullOrEmpty()) {
            startActivity<LoginActivity>()
        }

        setSupportActionBar(mainActivityToolbar)
        supportActionBar?.title = getString(R.string.main_toolbar_title)
        mainActivityViewPager.adapter = MainPagerAdapter(this, 2, supportFragmentManager)
        mainActivityTabLayout.setupWithViewPager(mainActivityViewPager)
    }
}
