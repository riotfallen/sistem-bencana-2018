package com.belarizki.sistembencana2018.activity.authentication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.belarizki.authentication.model.AuthResponse
import com.belarizki.authentication.presenter.AuthPresenter
import com.belarizki.authentication.utils.PrefConfig
import com.belarizki.authentication.view.AuthView
import com.belarizki.core.utils.gone
import com.belarizki.core.utils.visible
import com.belarizki.sistembencana2018.R
import com.belarizki.sistembencana2018.activity.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity

class LoginActivity : AppCompatActivity(), AuthView {

    private lateinit var authPresenter: AuthPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        authPresenter = AuthPresenter(this)
        loginActivityButtonLogin.setOnClickListener {
            val email = loginActivityEditTextEmail.text.toString()
            val password = loginActivityEditTextPassword.text.toString()
            authPresenter.getAuth(email, password)
        }

        loginActivityButtonRegisterHere.setOnClickListener {
            startActivity<RegisterActivity>()
        }
    }

    override fun showAuthLoading() {
        loginActivityProgressBar.visible()
        loginActivityButtonLogin.gone()
    }

    override fun hideAuthLoading() {
        loginActivityProgressBar.gone()
        loginActivityButtonLogin.visible()
    }

    override fun showFailureMessege(messege: String, errorCode: Int) {
        snackbar(activityLoginContent, messege).show()
    }

    override fun showAuthData(data: AuthResponse) {
        if (data.error){
            snackbar(activityLoginContent, data.messege)
        } else {
            data.auth?.token?.let { PrefConfig(this).writeTokenLogin(it) }
            if (PrefConfig(this).readTokenLogin() != ""){
                startActivity<MainActivity>()
            } else {
                snackbar(activityLoginContent, "Ups. Something went wrong")
            }
        }
    }
}
