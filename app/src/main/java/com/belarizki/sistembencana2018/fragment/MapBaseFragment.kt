package com.belarizki.sistembencana2018.fragment


import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.belarizki.sistembencana2018.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.fragment_map_base.*
import org.jetbrains.anko.support.v4.toast

class MapBaseFragment : Fragment(), OnMapReadyCallback {

    private lateinit var googleMap: GoogleMap

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map_base, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentMapBaseGoogleMap.onCreate(savedInstanceState)
        fragmentMapBaseGoogleMap.getMapAsync(this)
    }


    override fun onMapReady(map: GoogleMap) {
        this.googleMap = map
        googleMap.uiSettings.isMyLocationButtonEnabled = false

        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            googleMap.isMyLocationEnabled = true
        } else {
            toast("You was rejecting permission location")
        }

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(43.1, -87.9)))
    }
}
