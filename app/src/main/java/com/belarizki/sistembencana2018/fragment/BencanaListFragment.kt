package com.belarizki.sistembencana2018.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.belarizki.core.model.bencana.Bencana
import com.belarizki.core.presenter.BencanaPresenter
import com.belarizki.core.utils.invisible
import com.belarizki.core.utils.visible
import com.belarizki.core.view.bencana.BencanaView

import com.belarizki.sistembencana2018.R
import com.belarizki.sistembencana2018.adapter.recyclerview.MainRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_bencana_list.*
import org.jetbrains.anko.support.v4.toast

class BencanaListFragment : Fragment(), BencanaView {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bencana_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val presenter = BencanaPresenter(this)
        presenter.getBencana()
    }
    override fun showBencanaLoading() {
        fragmentBencanaListProgressBar.visible()
        fragmentBencanaListRecyclerView.invisible()
    }

    override fun hideBencanaLoading() {
        fragmentBencanaListProgressBar.invisible()
        fragmentBencanaListRecyclerView.visible()
    }

    override fun showBencanaError(message: String, errorCode: Int) {
        toast(message)
    }

    override fun showBencanaData(data: MutableList<Bencana>) {
        fragmentBencanaListRecyclerView.adapter = MainRecyclerAdapter(requireContext(), data)
        fragmentBencanaListRecyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayout.VERTICAL, false)
    }
}
